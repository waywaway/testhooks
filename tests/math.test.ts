import { sum, sub } from '../math';

it('sum', () => {
  expect(sum(1, 2)).toBe(3);
});

it('sub', () => {
  expect(sub(5, 1)).toBe(4);
});
